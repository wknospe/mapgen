
import java.awt.Color;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
class Flora {
    String typeName;
    static Random random = new Random();
    float growthRate;
    float lowHumidity;
    float highHumidity;
    float lowTemperature;
    float highTemperature;
    public Flora(String typeName, float lowHumidity, float highHumidity, float lowTemperature, float highTemperature, float growthRate) {
        this.typeName = typeName;
        this.lowHumidity = lowHumidity;
        this.highHumidity = highHumidity;
        this.lowTemperature = lowTemperature;
        this.highTemperature = highTemperature;
        this.growthRate = growthRate;
    }
    void checkPropagation(Loca newPlace) {
        if (!newPlace.terrain.land) {
            return;
        }
        float hFit = Math.max(lowHumidity - newPlace.humidity,newPlace.humidity - highHumidity);
        float tFit = Math.max(lowTemperature - newPlace.temperature, newPlace.temperature - highTemperature);
        if (hFit + tFit < 0) {
            if (random.nextInt(20) < growthRate*Math.abs(tFit + hFit)) {
                newPlace.flora.add(this);
            }
        }
    }
    boolean satisfies(Loca newPlace) {
        float hFit = Math.max(lowHumidity - newPlace.humidity,newPlace.humidity - highHumidity);
        float tFit = Math.max(lowTemperature - newPlace.temperature, newPlace.temperature - highTemperature);
        return (hFit < 0 && tFit < 0);
    }
    Color color() {
        switch (typeName) {
            case "decidous": return new Color(25, 124, 22);
            case "tropical": return new Color(39, 249, 22);
        }
        return new Color(25, 124, 22);
    }
}
