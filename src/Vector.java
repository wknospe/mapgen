
import java.util.List;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class Vector {
    int x;
    int y;
    List<Vector> unitQueue = new LinkedList();
    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
        if (!isUnitVector()) {
            generateUnitQueue();
        }
    }
    public boolean isUnitVector() {
        return (Math.abs(x) + Math.abs(y) <= 1);
    }
    public void generateUnitQueue() {
        unitQueue.clear();
        int incrX = Integer.signum(x);
        int incrY = Integer.signum(y);
        for (int i = x; i != 0; i = i - incrX) {
            unitQueue.add(new Vector(incrX, 0));
        }
        for (int i = y; i != 0; i = i - incrY) {
            unitQueue.add(new Vector(0, incrY));
        }
        Collections.shuffle(unitQueue, new Random());
    }
    public Vector getUnit() {
        if (isUnitVector()) {
            return this;
        }
        if (unitQueue.isEmpty() && !isUnitVector()) {
            generateUnitQueue();
        }
        Vector ret = unitQueue.remove(unitQueue.size() - 1);
        return ret;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
}
