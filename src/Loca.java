
import java.awt.Color;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
class Loca {
    Set<Loca> adjacents;
    float height;
    float cachedHeight;
    float temperature;
    float humidity = -1;
    float cachedHumidity;
    Set<Flora> flora;
    int x;
    int y;
    Terrain terrain;
    int calledAt = 0;
    public Loca(int x, int y) {
        this.adjacents = new HashSet<Loca>();
        this.flora = new HashSet<Flora>();
        this.x = x;
        this.y = y;
    }
    public void addAdjacent(Loca adjacent) {
        adjacents.add(adjacent);
    }
    public void flattenToAdjacents() {
        float sum = 0;
        for (Loca adjacent: adjacents) {
            sum = sum + adjacent.height;
        }
        float avg = sum/adjacents.size();
        cachedHeight = (float) (height + (avg - height)/(height/50 + 1));
    }
    public void deCache() {
        height = cachedHeight;
    }
    public void deCacheHumidity() {
        humidity = cachedHumidity;
    }
    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }
    public Color getTerrain() {
        return this.terrain.getColor();
    }
    public Color getTempMap() {
        int dispTemp;
        if (temperature < 0) {
            dispTemp = 0;
        } else if (temperature > 120) {
            dispTemp = 120;
        } else {
            dispTemp = (int) temperature;
        }
        return new Color(dispTemp,0, 120 - dispTemp);
    }
    public Color getHumidityMap() {
        int dispHumidity;
        if (humidity < 0) {
            dispHumidity = 0;
        } else if (humidity > 255) {
            dispHumidity = 255;
        } else {
            dispHumidity = (int) humidity;
        }
        return new Color(0, dispHumidity, 0);
    }
    public Color getHeightMap() {
        int dispHeight;
        if (height > 255) {
            dispHeight = 255;
        } else {
            dispHeight = (int) height;
        }
        int red, blue;
        if (dispHeight > 64) {
            blue = dispHeight - 64;
            red = 0;
        } else  {
            red = 64 - dispHeight;
            blue = 0;
        }
        return new Color(red,0,blue);
    }
    public void createFreshwater() {
        updateHumidity(600, 5);
        if (terrain == Terrain.FRESHWATER) {
            return;
        }
        terrain = Terrain.FRESHWATER;
        Loca min = null;
        for (Loca adjacent: adjacents) {
            if (!"freshwater".equals(adjacent.terrain)) {
                if (min == null || adjacent.height < min.height) {
                    min = adjacent;
                } 
            }
        }
        if (min != null && min.terrain != Terrain.SEA) {
            min.createFreshwater();
        }
    }
    public void initializeHumidity() {
        this.humidity = (float) (120*(Math.pow(.997, height - 40))*Math.pow(.996, temperature));
    }
    public void updateHumidity(float factor, int depth) {
        calledAt = depth;
        if (this.terrain == Terrain.FRESHWATER) {
            this.humidity = 200;
        } else {
            this.humidity = (float) ((120*(Math.pow(.997, height - 40))*Math.pow(.996, temperature)) + 2*factor + 4*humidity)/7;
        }
        if (depth - 1 < 0) {
            return;
        }
        for (Loca adjacent: adjacents) {
            if (adjacent.calledAt < depth) {
                adjacent.updateHumidity(humidity, depth - 1);
            }
        }
        calledAt = 0;
    }
    public void flattenToAdjacentsHumidity() {
        float sum = 0;
        for (Loca adjacent: adjacents) {
            sum = sum + adjacent.humidity;
        }
        float avg = sum/adjacents.size();
        cachedHumidity = (float) (humidity + (avg - humidity)/(humidity/50 + 1));
    }
    public void propagateFlora() {
        for (Flora plant: flora) {
            for (Loca adjacent: adjacents) {
                plant.checkPropagation(adjacent);
            }
        }
    }
    public List<Color> getMods() {
        List<Color> mods = new LinkedList<Color>();
        for (Flora plant: flora) {
            mods.add(plant.color());
        }
        return mods;
    }
}
