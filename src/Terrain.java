
import java.awt.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class Terrain {
    public static final Terrain LAND = new Terrain("land",true,false,new Color(47,142,48));
    public static final Terrain SEA = new Terrain("sea",false,false, new Color(44,93,178));
    public static final Terrain MOUNTAIN = new Terrain("mountain",true,true, new Color(132,134,137));
    public static final Terrain SNOW_MOUNTAIN = new Terrain("snow mountain",true,true,new Color(249,252,250));
    public static final Terrain SNOW = new Terrain("snow",true,false, new Color(247,248,249));
    public static final Terrain TUNDRA = new Terrain("tundra",true,false,new Color(147, 164, 191));
    public static final Terrain FRESHWATER = new Terrain("freshwater",false,false,new Color(44,93,178));
    public static final Terrain DESERT = new Terrain("desert",true,false,new Color(244, 242, 119));
    public Terrain(String name, boolean land, boolean mountain, Color color) {
        this.name = name;
        this.land = land;
        this.mountain = mountain;
        this.color = color;
    }
    public Color getColor() {
        return this.color;
    }
    Color color;
    String name;
    boolean land;
    boolean mountain;
}
