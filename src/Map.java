
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.Random;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import java.awt.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class Map extends JPanel implements MouseListener {
    private int _timeSlice = 50;  // update every 50 milliseconds
    private int iterations = 0;
    private int nextRescale = 25;
    public static enum ViewType { 
        TERRAIN, TEMPERATURE, HEIGHT, HUMIDITY;
        private static ViewType[] vals = values();
        public ViewType next() {
            return vals[(this.ordinal()+1) % vals.length];
        }
        public ViewType prev() {
            int ord = this.ordinal() - 1;
            if (ord < 0) {
                ord = vals.length + ord;
            }
            return vals[ord];
        }
    }
    public static ViewType viewType = ViewType.TERRAIN;
    public static final int SEALEVEL = 47;
    public static final int MOUNTAINLEVEL = 200;
    private Timer _timer = new Timer(_timeSlice,  (e) -> this.update());
    Loca[][] locae;
    int width;
    int height;
    Random random = new Random();
    public Map() {
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("pressed LEFT"), "left");
        this.getActionMap().put("left", new AbstractAction("left") { 
            public void actionPerformed(ActionEvent evt) {
                viewModeBack();
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("pressed RIGHT"), "right");
        this.getActionMap().put("right", new AbstractAction("right") { 
            public void actionPerformed(ActionEvent evt) {
                viewModeForwards();
            }
        });
        generateLocae();
    }
    public void generateLocae() {
        locae = new Loca[400][200];
        width = locae.length;
        height = locae[0].length;
        for (int i = 0; i < locae.length; i++) {
            for (int j = 0; j < locae[0].length; j++) {
                locae[i][j] = new Loca(i,j);
                locae[i][j].height = 64;
            }
        }
        for (int i = 0; i < locae.length; i++) {
            for (int j = 0; j < locae[0].length; j++) {
                for (int i2 = -1; i2 < 2; i2++) {
                    for (int j2 = -1; j2 < 2; j2++) {
                        if ((i2 != 0 || j2 != 0) && 
                            i2 + i < locae.length &&
                            i2 + i >= 0 && 
                            j2 + j < locae[0].length &&
                            j2 + j >= 0) {
                            locae[i][j].addAdjacent(locae[i + i2][j + j2]);
                        }
                    }
                }
            }
        }
        System.out.println(locae[1][1].adjacents.size());
        generateHeightMap();
        setSeas(SEALEVEL);
        setMountains(MOUNTAINLEVEL);
        setTemperatures();
        setSnow();
        makeRivers();
        createDeserts();
        createForests();
        _timer.start();
    }
    public void update() {
        repaint();
    }
    public void generateHeightMap() {
        for (int i = 0; i < 15; i++) {
            generateMountainRange();
            generateValleyRegion();
        }
        disturbTerrain();
        for (int j = 0; j < 2; j++) {
            for (int i = 0; i < 25; i++) {
                flattenTerrain();
            }
            disturbTerrain();
            rescaleHeight();
        }
        for (int i = 0; i < 25; i++) {
            flattenTerrain();
        }
        disturbTerrain();
        rescaleHeight();
        for (int i = 0; i < 5; i++) {
            generateMountainRange();
            generateValleyRegion();
        }
        for (int j = 0; j < 2; j++) {
            for (int i = 0; i < 25; i++) {
                flattenTerrain();
            }
            if (j == 0) {
                disturbTerrain();
            }
            rescaleHeight();
        }
    }
    public void generateValleyRegion() {
        int slope = -random.nextInt(4) - 4;
        int x = random.nextInt(locae.length);
        int y = random.nextInt(locae[0].length);
        byte curHeight = 64;
        Vector direction = new Vector(random.nextInt(13) - 6, random.nextInt(13) - 6);
        int length = random.nextInt(300) + 120;
        for (int i = 0; i < length; i++) {
            locae[x][y].height = curHeight;
            if (curHeight + slope <= 64 && curHeight + slope >= 0) {
                curHeight = (byte) (curHeight + slope);
            } else if (curHeight + slope > 64) {
                break;
            } else if (curHeight + slope < 0) {
                slope = slope + 1;
            }
            if (random.nextInt(length) < i) {
                slope = slope + 1;
            } else if (random.nextInt(8) == 0) {
                slope = slope - 1;
            }
            Vector unit = direction.getUnit();
            x = x + unit.x;
            y = y + unit.y;
            if (random.nextInt(5) == 0) {
                direction.setX(direction.x + random.nextInt(3) - 1);
                direction.setY(direction.y + random.nextInt(3) - 1);
            }
            if (x >= width || y >= height || x < 0 || y < 0) {
                break;
            }
        }
    }
    public void generateMountainRange() {
        int slope = random.nextInt(4) + 4;
        int x = random.nextInt(locae.length);
        int y = random.nextInt(locae[0].length);
        byte curHeight = 64;
        Vector direction = new Vector(random.nextInt(13) - 6, random.nextInt(13) - 6);
        int length = random.nextInt(300) + 120;
        for (int i = 0; i < length; i++) {
            locae[x][y].height = curHeight;
            if (curHeight + slope < 127 && curHeight + slope >= 64) {
                curHeight = (byte) (curHeight + slope);
            } else if (curHeight + slope >= 127) {
                slope = slope - 1;
            } else if (curHeight + slope < 64) {
                break;
        }
            if (random.nextInt(length) < i) {
                slope = slope - 1;
            } else if (random.nextInt(8) == 0) {
                slope = slope + 1;
            }
            Vector unit = direction.getUnit();
            x = x + unit.x;
            y = y + unit.y;
            if (random.nextInt(5) == 0) {
                direction.setX(direction.x + random.nextInt(3) - 1);
                direction.setY(direction.y + random.nextInt(3) - 1);
            }
            if (x >= width || y >= height || x < 0 || y < 0) {
                break;
            }
        }
    }
    public void flattenTerrain() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                locae[i][j].flattenToAdjacents();
            }
        }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                locae[i][j].deCache();
            }
        }
    }
    public void rescaleHeight() {
        float max = 0;
        float min = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (locae[i][j].height > max) {
                    max = locae[i][j].height;
                }
                if (locae[i][j].height < min) {
                    min = locae[i][j].height;
                }
            }
        }
        float ratio = (max - min)/300;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                locae[i][j].height = 65 + ((locae[i][j].height - 65)/ratio);
                if (locae[i][j].height > 255) {
                    locae[i][j].height = 255;
                }
                if (locae[i][j].height < 0) {
                    locae[i][j].height = 0;
                }
            }
        }
    }
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width*3, height*3);
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                switch (viewType) {
                    case TERRAIN: g2d.setColor(locae[i][j].getTerrain()); break;
                    case HEIGHT: g2d.setColor(locae[i][j].getHeightMap()); break;
                    case HUMIDITY: g2d.setColor(locae[i][j].getHumidityMap()); break;
                    case TEMPERATURE: g2d.setColor(locae[i][j].getTempMap()); break;
                    default: g2d.setColor(locae[i][j].getTerrain());
                }
                g2d.fillRect(3*i, 3*j, 3, 3);
                if (viewType.equals(ViewType.TERRAIN)) {
                    int sqW = 0;
                    int sqH = 0;
                    List<Color> mods = locae[i][j].getMods();
                    for (Color color: mods) {
                        g2d.setColor(color);
                        g2d.fillRect(3*i + sqW, 3*j + sqH, 1,1);
                        sqW++;
                        if (sqW == 3) {
                            sqW = 0;
                            sqH++;
                        }
                    }
                }
            }
        }
        g2d.dispose();
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        int xLoc = e.getX();
        int yLoc = e.getY() - 25;
        System.out.println(locae[xLoc/3][yLoc/3].terrain);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    private void setSeas(int seaLevel) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (locae[i][j].height < seaLevel) {
                    locae[i][j].setTerrain(Terrain.SEA);
                } 
                else {
                    locae[i][j].setTerrain(Terrain.LAND);
                }
            }
        }
    }

    private void setMountains(int mountainLevel) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (locae[i][j].height > mountainLevel) {
                    locae[i][j].setTerrain(Terrain.MOUNTAIN);
                } 
            }
        }
    }
    private void setTemperatures() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                float polarity = Math.min(height - j, j);
                float heightMod;
                if (locae[i][j].terrain == Terrain.SEA) {
                    heightMod = 0;
                } else {
                    heightMod = locae[i][j].height - SEALEVEL;
                }
                locae[i][j].temperature = (float) (137 - .16*heightMod - .5*((height - polarity)*(200/height)));
            }
        }
    }
    private void setSnow() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (locae[i][j].temperature < 32 && locae[i][j].terrain != Terrain.SEA) {
                    if (locae[i][j].terrain == Terrain.MOUNTAIN) {
                        locae[i][j].setTerrain(Terrain.SNOW_MOUNTAIN);
                    } else {
                        locae[i][j].setTerrain(Terrain.SNOW);
                    }
                } else if (locae[i][j].temperature < 45 && locae[i][j].terrain != Terrain.MOUNTAIN
                        && locae[i][j].terrain != Terrain.SEA) {
                    locae[i][j].setTerrain(Terrain.TUNDRA);
                }
            }
        }
    }
    private void makeRivers() {
        List<Loca> mountainLocae = new LinkedList<Loca>();
        for (int i =0; i<width; i++) {
            for (int j = 0; j<height;j++) {
                if (locae[i][j].height > 130) {
                    mountainLocae.add(locae[i][j]);
                }
                locae[i][j].initializeHumidity();
            }
        }
        for (int i = 0; i<15; i++) {
            createRiverSource(mountainLocae);
        }
        for (int i = 0; i<30; i++) {
            flattenHumidity();
        }
    }
    private void createRiverSource(List<Loca> mountainLocae) {
        mountainLocae.get(random.nextInt(mountainLocae.size())).createFreshwater();
    }
    private void flattenHumidity() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                locae[i][j].flattenToAdjacentsHumidity();
            }
        }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                locae[i][j].deCacheHumidity();
            }
        }
    }
    private void createDeserts() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (locae[i][j].temperature > 70 && locae[i][j].humidity < 80 && 
                    locae[i][j].terrain != Terrain.SEA && locae[i][j].terrain != Terrain.FRESHWATER) {
                    locae[i][j].setTerrain(Terrain.DESERT);
                }
            }
        }
    }
    private void disturbTerrain() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                locae[i][j].height = locae[i][j].height + random.nextInt(33) - 17;
            }
        }
    }
    public void viewModeBack() {
        System.out.println("called");
        viewType = viewType.next();
        System.out.println(viewType);
    }
    public void viewModeForwards() {
        System.out.println("called");
        viewType = viewType.prev();
        System.out.println(viewType);
    }
    private void createForests() {
        Flora forest = new Flora("decidous", 80, 200,32,70, .75f);
        Flora jungle = new Flora("tropical", 110, 200,65,150, .75f);
        List<Loca> satisfiedForest = new LinkedList<Loca>();
        List<Loca> satisfiedJungle = new LinkedList<Loca>();
        for (int i=0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (forest.satisfies(locae[i][j])) {
                    satisfiedForest.add(locae[i][j]);
                }
                if (jungle.satisfies(locae[i][j])) {
                    satisfiedJungle.add(locae[i][j]);
                }
            }
        }
        for (int i=0; i<10; i++) {
            try {
                satisfiedForest.get(random.nextInt(satisfiedForest.size())).flora.add(forest);
                satisfiedJungle.get(random.nextInt(satisfiedJungle.size())).flora.add(jungle);
            } catch (IllegalArgumentException e) {
                break;
            }
        }
        for (int i=0; i < 10; i++) {
            propagateFlora();
        }
    }
    private void propagateFlora() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                locae[i][j].propagateFlora();
            }
        }
    }
}
